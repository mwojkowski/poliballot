import csv
import sys
import time
import requests

import json

from BeautifulSoup import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf-8')

def main():

	#statescrape()
	FedScrape()


def statescrape():

	key = "######" #openstates API key

	states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI",
			  "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI",
			  "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC",
			  "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT",
			  "VT", "VA", "WA", "WV", "WI", "WY"] #all 50 states for scraping


	for state in states:
		DefaultLink = "http://openstates.org/api/v1/metadata/" + state +  "/?apikey=" + key
		try:
			r = requests.get(DefaultLink)
			print r.content
		except(ValueError):
			print "Unable to scrape information for state: " + state




	r = requests.get("http://openstates.org/api/v1/metadata/IL/?apikey=" + key)
	jsonTemp = json.loads(r.content)
	print jsonTemp

	print 'done'




def FedScrape():
	congressNumbers = ["110", "111", "112", "113", "114"]#only scraping from 110-114


	defaultLink = "https://www.govtrack.us/data/congress/"

	amendments = []

	bills = []

	votes = []

	legislators = []

	csvs = ["legislators-current.csv", "legislators-historic.csv"]


	tempDefault = ""


	newLink = ""


	for file in csvs:
		f = open(file, 'rb') # opens the csv file
		try:
			reader = csv.reader(f)
			for row in reader:
				print row

		except ValueError:
			print ValueError

		finally:
			f.close()




	for num in congressNumbers:#open each number of congress
		newLink = defaultLink + "" + num + "/" #reset the link so we arent adding to the old, already used link

		try:#try except statement to ensure the links dont kill the process


			#amendments
			tempDefault = newLink + "amendments/"#full link to ammendmants available through this link in every congress so far
			print("Now Visiting: " + tempDefault)

			r = requests.get(tempDefault)
			soup = BeautifulSoup(r.content)
			for a in soup.findAll("a"):
				if not((str(a.get("href"))).startswith('.')):
					r = requests.get(tempDefault + a.get("href"))		#here we are visiting https://www.govtrack.us/data/congress/amendments/hamdt or samdt
					print("visiting: " + tempDefault + a.get("href"))  #therefore this is senate and house for loop
					soup1 = BeautifulSoup(r.content)
					for b in soup1.findAll("a"):
						if not((str(b.get("href"))).startswith('.')):
							r = requests.get(tempDefault + a.get("href") + "/" + b.get("href") + "/data.json")
							json_temp = json.loads(r.content)
							amendments.append(Amendment(json_temp['actions']['acted_at'], json_temp['actions']['references']['refence'],
														json_temp['actions']['references']['type'], json_temp['actions']['text']),
											  			json_temp['actions']['type'], json_temp['actions']['vote_type'], )

							#amendments.append(Amendment(json_temp['actions']['acted_at'], ))
							print json_temp#prints each amendment json file, Need to upload this to sql database later


							#latestAction, reference, ref_type, action_type, action_at, how,
			   #references, result, roll, text, type, vote_type, where, amendment_id,
			   #amendment_type, amends_Amendment, amends_Bill, amends_bill_id, amends_bill_type,
			   #amends_congress, amends_number, amends_treaty, chamber, congress, description, house_number,
			   #number, introduced_date, purpose, sponsor_district, sponsor_name,
			   #sponsor_state, sponsor_thomasID, sponsor_title, sponsor_type,
			   #status, status_at, updated_at


			#bills
			tempDefault = newLink + "bills/"
			print("Now Visiting: " + tempDefault)
			r = requests.get(tempDefault)
			soup = BeautifulSoup(r.content)
			for a in soup.findAll("a"):
				if not((str(a.get("href"))).startswith('.')):
					r = requests.get(tempDefault + a.get("href") + "/")
					soup = BeautifulSoup(r.content)
					for b in soup.findAll("a"):
						if not((str(b.get("href"))).startswith('.')):
							r = requests.get(tempDefault + a.get("href") + "/" + b.get("href") + "/data.json")
							json_temp = json.loads(r.content)
							print json_temp#prints each bill json file, Need to upload this to sql database later


			#votes
			tempDefault = newLink + "votes/"
			print("Now Visiting: " + tempDefault)
			r = requests.get(tempDefault)
			soup = BeautifulSoup(r.content)
			for a in soup.findAll("a"):
				if not((str(a.get("href"))).startswith('.')):
					r = requests.get(tempDefault + a.get("href") + "/")
					soup = BeautifulSoup(r.content)
					for b in soup.findAll("a"):
						if not((str(b.get("href"))).startswith('.')):
							r = requests.get(tempDefault + a.get("href") + "/" + b.get("href") + "/data.json")
							json_temp = json.loads(r.content)
							print json_temp#prints each votes json file, Need to upload this to sql database later

		except ValueError:
			print("Unable to open and scrape: \n " + newLink + "\n" + ValueError)


class Amendment:
	latestAction = ""
	reference = ""
	ref_type = ""
	action_text = ""
	action_type = ""

	action_at=""
	how = ""
	references = []
	result = ""
	roll = 0
	text = ""
	type = ""
	vote_type = ""
	where = ""
	amendment_id = ""
	amendment_type = ""
	amends_Amendment = ""
	amends_Bill = ""
	amends_bill_id = ""
	amends_bill_type = ""
	amends_congress = ""
	amends_number = ""
	amends_treaty = ""
	chamber = ""
	congress = ""
	description = ""
	house_number = ""
	number = ""
	introduced_date = ""
	purpose = ""
	sponsor_district = ""
	sponsor_name = ""
	sponsor_state = ""
	sponsor_thomasID = ""
	sponsor_title = ""
	sponsor_type = ""
	status = ""
	status_at = ""
	updated_at = ""


	def __init__(self, latestAction, reference, ref_type, action_type, action_at, how,
			   references, result, roll, text, type, vote_type, where, amendment_id,
			   amendment_type, amends_Amendment, amends_Bill, amends_bill_id, amends_bill_type,
			   amends_congress, amends_number, amends_treaty, chamber, congress, description, house_number,
			   number, introduced_date, purpose, sponsor_district, sponsor_name,
			   sponsor_state, sponsor_thomasID, sponsor_title, sponsor_type,
			   status, status_at, updated_at):
		self.latestAction = latestAction
		self.reference = reference
		self.ref_type = ref_type
		self.action_type = action_type
		self.action_at = action_at
		self.how = how
		self.result = result
		self.roll = roll
		self.text = text
		self.type = type
		self.vote_type = vote_type
		self.where = where
		self.amendment_id = amendment_id
		self.amendment_type = amendment_type
		self.amends_Amendment = amends_Amendment
		self.amends_Bill = amends_Bill
		self.amends_bill_id = amends_bill_id
		self.amends_bill_type = amends_bill_type
		self.amends_congress = amends_congress
		self.amends_number = amends_number
		self.amends_treaty = amends_treaty
		self.chamber = chamber
		self.congress = congress
		self.description = description
		self.house_number = house_number
		self.number = number
		self.introduced_date = introduced_date
		self.purpose = purpose
		self.sponsor_district = sponsor_district
		self.sponsor_name = sponsor_name
		self.sponsor_state = sponsor_state
		self.sponsor_thomasID = sponsor_thomasID
		self.sponsor_title = sponsor_title
		self.sponsor_type = sponsor_type
		self.status = status
		self.status_at = status_at
		self.updated_at = updated_at

	def getlatestAction(self):
		return self.latestAction

	def getreference(self):
		return self.reference

	def getref_type(self):
		return self.ref_type

	def getaction_type(self):
		return self.action_type

	def getaction_at(self):
		return self.action_at

	def gethow(self):
		return self.how

	def getresult(self):
		return self.result

	def getroll(self):
		return self.roll

	def gettext(self):
		return self.text

	def gettype(self):
		return self.type

	def getvote_type(self):
		return self.vote_type

	def getwhere(self):
		return self.where

	def getamendment_id(self):
		return self.amendment_id

	def getamendment_type(self):
		return self.amendment_type

	def getamends_Amendment(self):
		return self.amends_Amendment

	def getamends_Bill(self):
		return self.amends_Bill

	def getamends_bill_id(self):
		return self.amends_bill_id

	def getamends_bill_type(self):
		return self.amends_bill_type

	def getamends_congress(self):
		return self.amends_congress

	def getamends_number(self):
		return self.amends_number

	def getamends_treaty(self):
		return self.amends_treaty

	def getchamber(self):
		return self.chamber

	def getcongress(self):
		return self.congress

	def getdescription(self):
		return self.description

	def gethouse_number(self):
		return self.house_number

	def getnumber(self):
		return self.number

	def getintroduced_date(self):
		return self.introduced_date

	def getpurpose(self):
		return self.purpose

	def getsponsor_district(self):
		return self.sponsor_district

	def getsponsor_name(self):
		return self.sponsor_name

	def getsponsor_state(self):
		return self.sponsor_state

	def getsponsor_thomasID(self):
		return self.sponsor_thomasID

	def getsponsor_title(self):
		return self.sponsor_title

	def getsponsor_type(self):
		return self.sponsor_type

	def getstatus(self):
		return self.status

	def getstatus_at(self):
		return self.status_at

	def getupdated_at(self):
		return self.updated_at


	def addReference(self, reference):
		self.references.append(reference)


class Legislator:
	lastName = ""
	firstName = ""
	birthday = ""
	gender = ""
	position = ""   #this should be known as 'type' in the legislator csv file
	state = ""
	district = ""
	party = ""
	url = ""
	address = ""
	phone = ""
	twitter = ""
	thomasID = ""
	govtrack_id = ""

	def __init__(self, lastName, firstName, birthday, gender, position, state, district,
				 party, url, address, phone, twitter, thomasID, govtrack_id):
		self.lastName = lastName
		self.firstName = firstName
		self.birthday = birthday
		self.gender = gender
		self.position = position
		self.state = state
		self.district = district
		self.party = party
		self.url = url
		self.address = address
		self.phone = phone
		self.twitter = twitter
		self.thomasID = thomasID
		self.govtrack_id = govtrack_id


	def getLastName(self):
		return self.lastName

	def getFirstName(self):
		return self.firstName

	def getBirday(self):
		return self.birthday

	def getGender(self):
		return self.gender

	def getPosition(self):
		return self.position

	def getState(self):
		return self.state

	def getDistrict(self):
		return self.district

	def getParty(self):
		return self.party

	def getURL(self):
		return self.url

	def getAddress(self):
		return self.address

	def getPhone(self):
		return self.phone

	def getTwitter(self):
		return self.twitter

	def getThomasID(self):
		return self.thomasID

	def getGovtrackID(self):
		return self.govtrack_id


main()